## 微信公众号模板消息定时发送平台
#### 系统特色：
###### 1.可管理多个公众号
###### 2.自定义模板消息，并带有模板消息记忆功能
###### 3.定时发送功能
#### 实用人群:
###### 企业运营人员/系统推广人员
###### 登陆账号密码 admin/admin
1.主界面
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/135919_af215560_344809.png "屏幕截图.png")

2.公众号消息界面
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/140043_58c3c450_344809.png "屏幕截图.png")

3.新增消息设置，设置发送时间，可设置三个用户（openid）进行预览
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/140136_70fcab0f_344809.png "屏幕截图.png")

# 求支持一下原创
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/142010_d7be7a8e_344809.jpeg "微信图片_20190530141614.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0530/142149_650c167d_344809.jpeg "微信图片_20190530141624.jpg")