package com.cjp.run.interceptor;

import java.io.File;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.cjp.utils.FileUtils;

/**
 * 自定义拦截器1
 */
public class MyInterceptor1 implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
    	System.out.println(request.getRequestURI());
    	if(request.getRequestURI().endsWith("/error")){
    		return false;
    	}
    	if(request.getRequestURI().endsWith("/login.html")||request.getRequestURI().endsWith("/loginService")||request.getRequestURI().endsWith("/logout")){
    		return true;
    	}
    	Object user=request.getSession().getAttribute("user");
    	if(user==null){
    		response.sendRedirect("login.html");
    	}else{
    		File file=new File(user.toString());
    		if(!file.exists()){
    			file.mkdir();
    		}
    		FileUtils.currentUser.set(user.toString());
    	}
        String appid=(String) request.getSession().getAttribute("currentAppId");
        String currentAppsecret=(String) request.getSession().getAttribute("currentAppsecret");
        if(appid!=null){
        	System.out.println("appid:"+appid+";currentAppsecret:"+currentAppsecret);
        	FileUtils.currentAppidThread.set(appid);
        	FileUtils.currentAppAppsecretThread.set(currentAppsecret);
        }
        return true;// 只有返回true才会继续向下执行，返回false取消当前请求
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
    }

}